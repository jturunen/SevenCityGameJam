﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodeGenerator : MonoBehaviour {

    public GameObject indicator;
    public GameObject indicatorLeft;
    public GameObject indicatorRight;
    public GameObject indicatorDown;

    static int MAX_CODE_LENGTH = 6;


    public List<string> p1Code = new List<string>();
    public List<string> p2Code = new List<string>();
    public List<string> p3Code = new List<string>();
    public List<string> p4Code = new List<string>();


    List<string> pool = new List<string>()
    {
        "a", "b", "x", "y",
    };

    public List<string> getRandomCode()
    {
        List<string> code = new List<string>();

        for (int i = 0; i < MAX_CODE_LENGTH; i++)
        {
            code.Add(pool[Random.Range(0, pool.Count)]);
        }

        return code;
    }

    public string listToString(List<string> codeList)
    {
        string codeStr = "";

        foreach (string s in codeList)
        {
            codeStr += s;
        }

        return codeStr;
    }

    public void Awake()
    {
        p1Code = getRandomCode();
        p2Code = getRandomCode();
        p3Code = getRandomCode();
        p4Code = getRandomCode();
    }

    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.S))
        {
            string codeStr = "";
            List<string> codeList = getRandomCode();

            foreach(string s in codeList)
            {
                codeStr += s;
            }

            Debug.Log("Random code: " + codeStr.ToLower());
        }
    }

    public void codeToSprites(List<string> code, GameObject indicatorHolder)
    {
        int counter = 0;
        foreach (string s in code)
        {
            GameObject objectToSpawn = Instantiate(getCorrectIndicatorSprite(s), new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            objectToSpawn.transform.parent = indicatorHolder.transform;
            objectToSpawn.transform.position = new Vector3(indicatorHolder.transform.position.x + counter, indicatorHolder.transform.position.y, indicatorHolder.transform.position.z);
            counter++;
        }
    }

   /* public void inputToSprite(string input, GameObject inputHolder, int counter)
    {

        GameObject objectToSpawn = Instantiate(getCorrectIndicatorSprite(input), new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
        objectToSpawn.transform.parent = gameObject.transform;
        objectToSpawn.transform.position = new Vector3(inputHolder.transform.position.x + counter, inputHolder.transform.position.y, inputHolder.transform.position.z);
        
    }*/

    public GameObject getCorrectIndicatorSprite(string s)
    {
        switch (s)
        {
            case "a":
                {
                    return indicatorDown;
                }
            case "b":
                {
                    return indicatorLeft;
                }
            case "y":
                {
                    return indicator;
                }
            case "x":
                {
                    return indicatorRight;
                }
            default:
                return null;
        }
    }
}
