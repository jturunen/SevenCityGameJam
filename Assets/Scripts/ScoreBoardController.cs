﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreBoardController : MonoBehaviour
{

    public TextMeshProUGUI scoreBoard;
    public TextMeshProUGUI yourScore;

    public List<float> allScores;
    public List<float> sortedScores;

    bool scoreMade = false;

    [SerializeField]
    private KeyCode resetKey;
    // Use this for initialization
    void Start()
    {
        //Template how scores should be saved
        /*PlayerPrefs.SetFloat("PlayerScore", 15);
        PlayerPrefs.SetFloat("Score1", 10);
        PlayerPrefs.SetFloat("Score2", 12);
        PlayerPrefs.SetFloat("Score3", 15);
        PlayerPrefs.SetFloat("Score4", 3);
        PlayerPrefs.SetFloat("Score5", 12);
        PlayerPrefs.SetFloat("Score6", 16);*/

        allScores = new List<float>();
        //Go through all floats from PlayerPrefs that were saved with a key: Score + number (Score1)
        for (int i = 0; i < 1000; i++)
        {
            float score = PlayerPrefs.GetFloat("Score" + i);

            allScores.Add(score);
        }
        //Order by descending
        sortedScores = allScores.OrderByDescending(item => item).ToList();
        
        //Update your score
        yourScore.text = "" + PlayerPrefs.GetFloat("PlayerScore");

        createScores();
    }

    private void createScores()
    {
        //Show top 5 scores
        for (int i = 0; i <= 4; i++)
        {
            float score = sortedScores[i];
            scoreBoard.text += i + 1 + ".      " + score + "\n";
        }
    }

    // Update is called once per frame
    void Update()
    {
        resetScores();
    }
    
    void resetScores()
    {
        if (Input.GetKeyDown(resetKey))
        {
            PlayerPrefs.DeleteAll();
        }
    }
}
