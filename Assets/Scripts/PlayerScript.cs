﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerScript : MonoBehaviour {

    public CodeGenerator codeGenerator;
    public SceneController sceneController;
    public ControllerScript controllerScript;

    public TextMeshPro codeText;
    public TextMeshProUGUI playerInputText;
    public TextMeshProUGUI playerNumText;
    public Image playerKIAImage;

    public List<string> code;

    public PlayerScript p1;
    public PlayerScript p2;
    public PlayerScript p3;
    public PlayerScript p4;

    public List<string> p1Code;
    public List<string> p2Code;
    public List<string> p3Code;
    public List<string> p4Code;

    [SerializeField]
    List<string> playerInput;

    [SerializeField]
    int inputCounter = 0;

    public bool dead = false;
    public bool canType = false;
    public bool shotSomeone = false;
    public bool gameOver = false;

    public AudioSource audioSource;
    AudioClip[] shootingPool;
    public AudioClip[] winPool;

    public player thisPlayerNum;

    public GameObject indicatorHolder;
    public GameObject inputHolder;

    public Sprite indicatorUp;
    public Sprite indicatorRight;
    public Sprite indicatorDown;
    public Sprite indicatorLeft;

    public Sprite xSprite;
    public Sprite empty;

    public Sprite p1Sprite;
    public Sprite p2Sprite;
    public Sprite p3Sprite;
    public Sprite p4Sprite;


    public enum player {
        player1 = 1,
        player2 = 2,
        player3 = 3,
        player4 = 4,
    };

    // Use this for initialization
    void Start()
    {
        dead = false;
        canType = false;

        playerInput = new List<string>();
        playerInputText.text = "";
        
        p1Code = codeGenerator.p1Code;
        p2Code = codeGenerator.p2Code;
        
        p3Code = codeGenerator.p3Code;
        p4Code = codeGenerator.p4Code;
        

        switch (thisPlayerNum)
        {
            case player.player1:
                code = p1Code;
                playerNumText.text = "Player 1";
                shootingPool = AudioManager.instance.shoot1SFXPool;
                winPool = AudioManager.instance.player1WinPool;
                break;

            case player.player2:
                code = p2Code;
                playerNumText.text = "Player 2";
                shootingPool = AudioManager.instance.shoot2SFXPool;
                winPool = AudioManager.instance.player2WinPool;
                break;
                
            case player.player3:
                code = p3Code;
                playerNumText.text = "Player 3";
                shootingPool = AudioManager.instance.shoot3SFXPool;
                winPool = AudioManager.instance.player3WinPool;
                break;

            case player.player4:
                code = p4Code;
                playerNumText.text = "Player 4";
                shootingPool = AudioManager.instance.shoot4SFXPool;
                winPool = AudioManager.instance.player4WinPool;
                break;
        }

        audioSource = gameObject.GetComponent<AudioSource>();

        codeText.text = codeGenerator.listToString(code);
        codeGenerator.codeToSprites(code, indicatorHolder);
    }

 

    private void Update()
    {
        if (shotSomeone)
        {
            AudioClip shootingSound = AudioManager.instance.getRandomSFX(shootingPool);
            AudioManager.instance.PlaySinglePlayer(shootingSound, audioSource);

            shotSomeone = false;
        }
    }

    private void isKill()
    {
        if (inputCounter == 6)
        {
            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(p1.code))
            {
                StartCoroutine(changeSprite());

                shotSomeone = true;
                p1.kill();
            }

            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(p2.code))
            {
                StartCoroutine(changeSprite());

                shotSomeone = true;
                p2.kill();
            }

            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(p3.code))
            {
                StartCoroutine(changeSprite());

                shotSomeone = true;
                p3.kill();
            }

            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(p4.code))
            {
                StartCoroutine(changeSprite());
                shotSomeone = true;
                p4.kill();
            }
            playerInput.Clear();
            inputCounter = 0;
            playerInputText.text = codeGenerator.listToString(playerInput);
        }
    }

    public void addToInput(string input)
    {
        AudioManager.instance.PlaySingle(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool));
        //playerKIAImage.gameObject.SetActive(false);
        
        playerInput.Add(input);
        playerInputText.text = codeGenerator.listToString(playerInput);
        changeInputSprite(input);
        //codeGenerator.inputToSprite(input, , inputCounter);
        inputCounter++;
        checkInput();
        isKill();
    }

    public void changeInputSprite(string input)
    {
        transform.GetChild(2).gameObject.GetComponent<SpriteRenderer>().sprite = getCorrectIndicatorSprite(input);
    }

    public Sprite getCorrectIndicatorSprite(string s)
    {
        switch (s)
        {
            case "a":
                {
                    return indicatorDown;
                }
            case "b":
                {
                    return indicatorLeft;
                }
            case "y":
                {
                    return indicatorUp;
                }
            case "x":
                {
                    return indicatorRight;
                }
            default:
                return null;
        }
    }

    public void checkInput()
    {
        bool p1missed = false;
        bool p2missed = false;
        bool p3missed = false;
        bool p4missed = false;

        List<string> tempP1Code = new List<string>();
        List<string> tempP2Code = new List<string>();
        List<string> tempP3Code = new List<string>();
        List<string> tempP4Code = new List<string>();
        for (int i = 0; i <= inputCounter-1; i++)
        {
            if (!p1.dead)
            {
                tempP1Code.Add(p1Code[i]);
            }
            if (!p2.dead)
            {
                tempP2Code.Add(p2Code[i]);
            }
            if (!p3.dead)
            {
                tempP3Code.Add(p3Code[i]);
            }
            if (!p4.dead)
            {
                tempP4Code.Add(p4Code[i]);
            }
        }


        if (!p1.dead)
        {
            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(tempP1Code))
            {
                p1missed = false;
            }
            else
            {
                p1missed = true;
            }
        } else
        {
            p1missed = true;
        }

        if (!p2.dead)
        {
            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(tempP2Code))
            {
                p2missed = false;
            }
            else
            {
                p2missed = true;
            }
        }
        else
        {
            p2missed = true;
        }

        if (!p3.dead)
        {
            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(tempP3Code))
            {
                p3missed = false;
            }
            else
            {
                p3missed = true;
            }
        }
        else
        {
            p3missed = true;
        }
        if (!p4.dead)
        {
            if (codeGenerator.listToString(playerInput) == codeGenerator.listToString(tempP4Code))
            {
                p4missed = false;
            }
            else
            {
                p4missed = true;
            }
        }
        else
        {
            p4missed = true;
        }
        if (p1missed && p2missed && p3missed && p4missed)
        {
            miss();
        }
      
    }

    private void miss()
    {
        transform.GetChild(2).gameObject.GetComponent<SpriteRenderer>().sprite = xSprite;
        AudioManager.instance.PlaySinglePlayer(AudioManager.instance.missTypeSFX, audioSource);
        playerInput.Clear();
        playerInputText.text = "";
        inputCounter = 0;

        //StartCoroutine(controllerScript.vibrateController());

        StartCoroutine(stun());
        //playerKIAImage.gameObject.SetActive(true);
    }

    public IEnumerator transcendWaitForAudio()
    {
        PlayerPrefs.SetString("player", thisPlayerNum.ToString());


        indicatorHolder.SetActive(false);
        inputHolder.SetActive(false);

        while (audioSource.isPlaying)
        {
            yield return null;
        }
        // audio has finished...
        transcend();
    }
    public IEnumerator transcendAnimation()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }
        int count = 0;
        gameOver = true; 

        switch (thisPlayerNum)
        {
            case player.player1:
                GameObject.Find("light_p1").GetComponent<SpriteRenderer>().enabled = true;
                break;

            case player.player2:
                GameObject.Find("light_p2").GetComponent<SpriteRenderer>().enabled = true;
                break;

            case player.player3:
                GameObject.Find("light_p3").GetComponent<SpriteRenderer>().enabled = true;
                break;

            case player.player4:
                GameObject.Find("light_p4").GetComponent<SpriteRenderer>().enabled = true;
                break;
        }

        AudioManager.instance.PlaySingle(AudioManager.instance.transcend);
        if(thisPlayerNum == player.player1 || thisPlayerNum == player.player2)
        {
            count = 130;
        } else
        {
            count = 75;
        }

        while (count >= 0)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y + 0.1f);
            yield return new WaitForSeconds(0.02f);
            count--;
        }
        // animation done
        StartCoroutine(changeToWinScene());
    }


    private void transcend()
    {
        AudioClip winSound = AudioManager.instance.getRandomSFX(winPool);
        AudioManager.instance.PlaySingle(winSound);

        Debug.Log("Youve become the Space Cowboy");
        //animation and end game

        StartCoroutine(transcendAnimation());
    }

    private IEnumerator changeToWinScene()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }


        sceneController.loadScene4();
    }

    public void kill()
    {
        if (!dead)
        {

            dead = true;
            canType = false;
            playerKIAImage.gameObject.SetActive(true);
            playerInputText.text = "KILLED";
            Debug.Log("KIA: " + thisPlayerNum);
            gameObject.SetActive(false);

            switch (thisPlayerNum)
            {
                case player.player1:
                    GameObject.Find("graves_2").GetComponent<SpriteRenderer>().enabled = true;
                    break;

                case player.player2:
                    GameObject.Find("graves_3").GetComponent<SpriteRenderer>().enabled = true;


                    break;

                case player.player3:
                    GameObject.Find("graves_0").GetComponent<SpriteRenderer>().enabled = true;


                    break;

                case player.player4:
                    GameObject.Find("graves_1").GetComponent<SpriteRenderer>().enabled = true;


                    break;
            }

        }
    }
    IEnumerator stun()
    {
        canType = false;
        yield return new WaitForSeconds(0.5f);
        transform.GetChild(2).gameObject.GetComponent<SpriteRenderer>().sprite = empty;
        canType = true;

    }

    IEnumerator changeSprite()
    {
        Sprite currentSprite = GetComponent<SpriteRenderer>().sprite;
        switch (thisPlayerNum)
        {
            case player.player1:
                GetComponent<SpriteRenderer>().sprite = p1Sprite;
                GameObject.Find("bang_p1").GetComponent<SpriteRenderer>().enabled = true;
                yield return new WaitForSeconds(0.5f);
                GameObject.Find("bang_p1").GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<SpriteRenderer>().sprite = currentSprite;
                break;

            case player.player2:
                GetComponent<SpriteRenderer>().sprite = p2Sprite;
                GameObject.Find("bang_p2").GetComponent<SpriteRenderer>().enabled = true;
                yield return new WaitForSeconds(0.5f);
                GameObject.Find("bang_p2").GetComponent<SpriteRenderer>().enabled = false;

                GetComponent<SpriteRenderer>().sprite = currentSprite;

                break;

            case player.player3:
                GetComponent<SpriteRenderer>().sprite = p3Sprite;
                GameObject.Find("bang_p3").GetComponent<SpriteRenderer>().enabled = true;
                yield return new WaitForSeconds(0.5f);
                GameObject.Find("bang_p3").GetComponent<SpriteRenderer>().enabled = false;

                GetComponent<SpriteRenderer>().sprite = currentSprite;

                break;

            case player.player4:
                GetComponent<SpriteRenderer>().sprite = p4Sprite;
                GameObject.Find("bang_p4").GetComponent<SpriteRenderer>().enabled = true;
                yield return new WaitForSeconds(0.5f);
                GameObject.Find("bang_p4").GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<SpriteRenderer>().sprite = currentSprite;

                break;
        }
    }

}
