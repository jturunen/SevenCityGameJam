﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour { 
    SceneController sceneController;
    private bool creditsClicked;
    private bool gameClicked;

    // Use this for initialization
    void Start()
    {
        AudioManager.instance.PlayLoopMusic(AudioManager.instance.menuMusic);
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();

        Screen.SetResolution(1280, 720, true);
    }

    public void buttonClickCredits()
    {
        if (!creditsClicked)
        {
            creditsClicked = true;
            AudioManager.instance.PlaySingle(AudioManager.instance.menuchoiseSFX);
            StartCoroutine(changeSceneToCredits());
        }
        

    }

    private IEnumerator changeSceneToCredits()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }

        sceneController.loadScene5();
    }

    public void buttonClickExit()
    {
        AudioManager.instance.PlaySingle(AudioManager.instance.menuchoiseSFX);
        StartCoroutine(closeGame());
    }

    private IEnumerator closeGame()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }

        sceneController.quitGame();
    }

    public void buttonClickGame()
    {
        if (!gameClicked)
        {
            gameClicked = true;
            AudioManager.instance.PlaySingle(AudioManager.instance.menuchoiseSFX);
            StartCoroutine(changeSceneToGame());
        }
    }

    private IEnumerator changeSceneToGame()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }

        sceneController.loadScene3();
    }

}
