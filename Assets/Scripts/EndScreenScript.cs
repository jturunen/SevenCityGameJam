﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScreenScript : MonoBehaviour {
    SceneController sceneController;

    public Image winningPlayerImage;
    string winningPlayer;
    private bool skipActivated;
    public Sprite winner1, winner2, winner3, winner4;

	// Use this for initialization
	void Start () {
        AudioManager.instance.PlayLoopMusic(AudioManager.instance.winMusic);

        Screen.SetResolution(1280, 720, true);

        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();

        winningPlayer = PlayerPrefs.GetString("player");

        setWinnerImage();
	}

    private void Update()
    {
        if(!skipActivated)
        {
            if (Input.anyKeyDown)
            {
                skipActivated = true;
                buttonClick();
            }
        }

    }

    void setWinnerImage()
    {
        switch(winningPlayer)
        {
            case "player1":
                winningPlayerImage.sprite = winner1;
                break;

            case "player2":
                winningPlayerImage.sprite = winner2;
                break;

            case "player3":
                winningPlayerImage.sprite = winner3;
                break;

            case "player4":
                winningPlayerImage.sprite = winner4;
                break;
        }
    }

    public void buttonClick()
    {
        AudioManager.instance.PlaySingle(AudioManager.instance.menuchoiseSFX);
        StartCoroutine(changeScene());
    }

    private IEnumerator changeScene()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }

        sceneController.loadScene2();
    }
}
