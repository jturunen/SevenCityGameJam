﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure; // Required in C#


public class ControllerScript : MonoBehaviour {
    public PlayerScript playerScript;
    public MainMenuScript mainMenuScript;
    public GameController gameController;
    private int playerAmount;

    public GameObject poster3, poster4;
    private int skipCounter;

    private void Start()
    {

        playerAmount = 2;
        playerAmount = PlayerPrefs.GetInt("playerAmount");
        if(mainMenuScript != null)
        {
            checkPlayerAmount();
        }
        int counter = 0;
        foreach(string s in Input.GetJoystickNames())
        {
            counter++;
            Debug.Log("" + counter + s);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerScript != null)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                playerScript.sceneController.loadScene2();
            }

            if (playerScript.canType)
            {
                switch (playerScript.thisPlayerNum)
                {
                    #region PLAYER 1 CONTROLS
                    case PlayerScript.player.player1:
                        if (Input.GetButtonDown("Con1B") || Input.GetKeyDown(KeyCode.A) )
                        {
                            Debug.Log("1B Rooting");
                            playerScript.addToInput("b");
                        }

                        if (Input.GetButtonDown("Con1X") || Input.GetKeyDown(KeyCode.D) )
                        {
                            Debug.Log("1X Tooting");
                            playerScript.addToInput("x");

                        }

                        if (Input.GetButtonDown("Con1A") || Input.GetKeyDown(KeyCode.S) )
                        {
                            Debug.Log("1A Shooting");
                            playerScript.addToInput("a");

                        }

                        if (Input.GetButtonDown("Con1Y") || Input.GetKeyDown(KeyCode.W))
                        {
                            Debug.Log("1Y Booting");
                            playerScript.addToInput("y");
                        }
                        break;
                    #endregion
                    #region PLAYER 2 CONTROLS
                    case PlayerScript.player.player2:
                        if (Input.GetButtonDown("Con2B") || Input.GetKeyDown(KeyCode.J))
                        {
                            Debug.Log("2B Rooting");
                            playerScript.addToInput("b");
                        }

                        if (Input.GetButtonDown("Con2X") || Input.GetKeyDown(KeyCode.L))
                        {
                            Debug.Log("2X Tooting");
                            playerScript.addToInput("x");

                        }

                        if (Input.GetButtonDown("Con2A") || Input.GetKeyDown(KeyCode.K))
                        {
                            Debug.Log("2A Shooting");
                            playerScript.addToInput("a");

                        }

                        if (Input.GetButtonDown("Con2Y") || Input.GetKeyDown(KeyCode.I))
                        {
                            Debug.Log("2Y Booting");
                            playerScript.addToInput("y");
                        }
                        break;
                    #endregion
                    #region PLAYER 3 CONTROLS
                    case PlayerScript.player.player3:
                        if (Input.GetButtonDown("Con3B") || Input.GetKeyDown(KeyCode.LeftArrow))
                        {
                            Debug.Log("3B Rooting");
                            playerScript.addToInput("b");
                        }

                        if (Input.GetButtonDown("Con3X") || Input.GetKeyDown(KeyCode.RightArrow))
                        {
                            Debug.Log("3X Tooting");
                            playerScript.addToInput("x");

                        }

                        if (Input.GetButtonDown("Con3A") || Input.GetKeyDown(KeyCode.DownArrow))
                        {
                            Debug.Log("3A Shooting");
                            playerScript.addToInput("a");

                        }

                        if (Input.GetButtonDown("Con3Y") || Input.GetKeyDown(KeyCode.UpArrow))
                        {
                            Debug.Log("3Y Booting");
                            playerScript.addToInput("y");
                        }
                        break;
                    #endregion
                    #region PLAYER 4 CONTROLS
                    case PlayerScript.player.player4:
                        if (Input.GetButtonDown("Con4B") || Input.GetKeyDown(KeyCode.F))
                        {
                            Debug.Log("4B Rooting");
                            playerScript.addToInput("b");
                        }

                        if (Input.GetButtonDown("Con4X") || Input.GetKeyDown(KeyCode.H))
                        {
                            Debug.Log("4X Tooting");
                            playerScript.addToInput("x");

                        }

                        if (Input.GetButtonDown("Con4A") || Input.GetKeyDown(KeyCode.G))
                        {
                            Debug.Log("4A Shooting");
                            playerScript.addToInput("a");

                        }

                        if (Input.GetButtonDown("Con4Y") || Input.GetKeyDown(KeyCode.T))
                        {
                            Debug.Log("4Y Booting");
                            playerScript.addToInput("y");
                        }
                        break;
                        #endregion
                }
            } else if (gameController != null && !gameController.gameStarted && !playerScript.canType)
            {
                switch (playerScript.thisPlayerNum)
                {
                    #region PLAYER 1 CONTROLS
                    case PlayerScript.player.player1:
                        if (Input.GetButtonDown("Con1B") || Input.GetKeyDown(KeyCode.A))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("b");
                        }

                        if (Input.GetButtonDown("Con1X") || Input.GetKeyDown(KeyCode.D))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("x");

                        }

                        if (Input.GetButtonDown("Con1A") || Input.GetKeyDown(KeyCode.S))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("a");

                        }

                        if (Input.GetButtonDown("Con1Y") || Input.GetKeyDown(KeyCode.W))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("y");
                        }
                        break;
                    #endregion
                    #region PLAYER 2 CONTROLS
                    case PlayerScript.player.player2:
                        if (Input.GetButtonDown("Con2B") || Input.GetKeyDown(KeyCode.J))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("b");
                        }

                        if (Input.GetButtonDown("Con2X") || Input.GetKeyDown(KeyCode.L))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("x");

                        }

                        if (Input.GetButtonDown("Con2A") || Input.GetKeyDown(KeyCode.K))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("a");

                        }

                        if (Input.GetButtonDown("Con2Y") || Input.GetKeyDown(KeyCode.I))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("y");
                        }
                        break;
                    #endregion
                    #region PLAYER 3 CONTROLS
                    case PlayerScript.player.player3:
                        if (Input.GetButtonDown("Con3B") || Input.GetKeyDown(KeyCode.LeftArrow))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("b");
                        }

                        if (Input.GetButtonDown("Con3X") || Input.GetKeyDown(KeyCode.RightArrow))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("x");

                        }

                        if (Input.GetButtonDown("Con3A") || Input.GetKeyDown(KeyCode.DownArrow))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);

                            playerScript.changeInputSprite("a");
                        }

                        if (Input.GetButtonDown("Con3Y") || Input.GetKeyDown(KeyCode.UpArrow))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("y");
                        }
                        break;
                    #endregion
                    #region PLAYER 4 CONTROLS
                    case PlayerScript.player.player4:
                        if (Input.GetButtonDown("Con4B") || Input.GetKeyDown(KeyCode.F))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("b");
                        }

                        if (Input.GetButtonDown("Con4X") || Input.GetKeyDown(KeyCode.H))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("x");
                        }

                        if (Input.GetButtonDown("Con4A") || Input.GetKeyDown(KeyCode.G))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("a");
                        }

                        if (Input.GetButtonDown("Con4Y") || Input.GetKeyDown(KeyCode.T))
                        {
                            AudioManager.instance.PlaySinglePlayer(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool), playerScript.audioSource);
                            playerScript.changeInputSprite("y");

                        }
                        break;
                        #endregion
                }


            }
        } else if (mainMenuScript != null)
        {
            #region PLAYER 1 CONTROLS
            if (Input.GetButtonDown("Con1B") || Input.GetKeyDown(KeyCode.A))
            {
                Debug.Log("1B Rooting");
                mainMenuScript.buttonClickCredits();
            }

            if (Input.GetButtonDown("Con1X") || Input.GetKeyDown(KeyCode.D))
            {
                Debug.Log("1X Tooting");
                mainMenuScript.buttonClickGame();

            }

            if (Input.GetButtonDown("Con1A") || Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("1A Shooting");
                addPlayers();

            }

            if (Input.GetButtonDown("Con1Y") || Input.GetKeyDown(KeyCode.W))
            {
                Debug.Log("1Y Booting");
                mainMenuScript.buttonClickExit();
            }
            #endregion

            #region PLAYER 2 CONTROLS

            if (Input.GetButtonDown("Con2B") || Input.GetKeyDown(KeyCode.J))
            {
                Debug.Log("2B Rooting");
                mainMenuScript.buttonClickCredits();
            }

            if (Input.GetButtonDown("Con2X") || Input.GetKeyDown(KeyCode.L))
            {
                Debug.Log("2X Tooting");
                mainMenuScript.buttonClickGame();

            }

            if (Input.GetButtonDown("Con2A") || Input.GetKeyDown(KeyCode.K))
            {
                Debug.Log("2A Shooting");
                addPlayers();

            }

            if (Input.GetButtonDown("Con2Y") || Input.GetKeyDown(KeyCode.I))
            {
                Debug.Log("2Y Booting");
                mainMenuScript.buttonClickExit();
            }
            #endregion

            #region PLAYER 3 CONTROLS
            if (Input.GetButtonDown("Con3B") || Input.GetKeyDown(KeyCode.LeftArrow))
            {
                Debug.Log("3B Rooting");
                mainMenuScript.buttonClickCredits();
            }

            if (Input.GetButtonDown("Con3X") || Input.GetKeyDown(KeyCode.RightArrow))
            {
                Debug.Log("3X Tooting");
                mainMenuScript.buttonClickGame();

            }

            if (Input.GetButtonDown("Con3A") || Input.GetKeyDown(KeyCode.DownArrow))
            {
                Debug.Log("3A Shooting");
                addPlayers();

            }

            if (Input.GetButtonDown("Con3Y") || Input.GetKeyDown(KeyCode.UpArrow))
            {
                Debug.Log("3Y Booting");
                mainMenuScript.buttonClickExit();
            }
            #endregion

            #region PLAYER 4 CONTROLS
            if (Input.GetButtonDown("Con4B") || Input.GetKeyDown(KeyCode.F))
            {
                Debug.Log("4B Rooting");
                mainMenuScript.buttonClickCredits();
            }

            if (Input.GetButtonDown("Con4X") || Input.GetKeyDown(KeyCode.H))
            {
                Debug.Log("4X Tooting");
                mainMenuScript.buttonClickGame();

            }

            if (Input.GetButtonDown("Con4A") || Input.GetKeyDown(KeyCode.G))
            {
                Debug.Log("4A Shooting");
                addPlayers();

            }

            if (Input.GetButtonDown("Con4Y") || Input.GetKeyDown(KeyCode.T))
            {
                Debug.Log("4Y Booting");
                mainMenuScript.buttonClickExit();
            }
            #endregion
        }

        if (mainMenuScript == null && playerScript != null && playerScript.gameOver)
        {
            if (Input.anyKeyDown)
            {
                skipCounter++;
                if(skipCounter == 3)
                {
                    Debug.Log("Skipcounter" + skipCounter);
                    skipCounter = 0;
                    playerScript.sceneController.loadScene4();
                }
            }
        }
        
    }
    public void addPlayers()
    {
        AudioManager.instance.PlaySingle(AudioManager.instance.getRandomSFX(AudioManager.instance.inputSFXPool));
        playerAmount++;
        if (playerAmount > 4)
        {
            playerAmount = 2;
        }
        PlayerPrefs.SetInt("playerAmount", playerAmount);
        checkPlayerAmount();
    }

    public void checkPlayerAmount()
    {
        switch (playerAmount)
        {
            case 2:
                poster3.SetActive(false);
                poster4.SetActive(false);
                break;
            case 3:
                poster3.SetActive(true);
                break;
            case 4:
                poster4.SetActive(true);
                break;
        }
    }

    /*public IEnumerator vibrateController()
    {
        switch (playerScript.thisPlayerNum)
        {
            case PlayerScript.player.player1:
                GamePad.SetVibration(PlayerIndex.Two, 0, 2);
                yield return new WaitForSeconds(0.35f);
                GamePad.SetVibration(PlayerIndex.Two, 0, 0);
                break;

            case PlayerScript.player.player2:
                GamePad.SetVibration(PlayerIndex.One, 0, 2);
                yield return new WaitForSeconds(0.35f);
                GamePad.SetVibration(PlayerIndex.One, 0, 0);
                break;

            case PlayerScript.player.player3:
                GamePad.SetVibration(PlayerIndex.Three, 0, 2);
                yield return new WaitForSeconds(0.35f);
                GamePad.SetVibration(PlayerIndex.Three, 0, 0);
                break;

            case PlayerScript.player.player4:
                GamePad.SetVibration(PlayerIndex.Four, 0, 2);
                yield return new WaitForSeconds(0.35f);
                GamePad.SetVibration(PlayerIndex.Four, 0, 0);
                break;

        }

    }*/
}
