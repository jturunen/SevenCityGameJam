﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour {
    SceneController sceneController;
    private bool clicked;

    // Use this for initialization
    void Start()
    {
        AudioManager.instance.PlayLoopMusic(AudioManager.instance.windBg);
        sceneController = GameObject.Find("SceneController").GetComponent<SceneController>();
        
        Screen.SetResolution(1280, 720, true);
    }

    private void Update()
    {
        if(Input.anyKey)
        {
            buttonClick();
        }
    }

    public void buttonClick()
    {
        if(!clicked)
        {
            clicked = true;
            AudioManager.instance.PlaySingle(AudioManager.instance.menuchoiseSFX);
            StartCoroutine(changeToMenu());
        }

    }

    private IEnumerator changeToMenu()
    {
        while (AudioManager.instance.sfxSource.isPlaying)
        {
            yield return null;
        }

        sceneController.loadScene2();
    }
}
