﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    [SerializeField]
    private string sceneName1;
    [SerializeField]
    private string sceneName2;
    [SerializeField]
    private string sceneName3;
    [SerializeField]
    private string sceneName4;
    [SerializeField]
    private string sceneName5;

    [SerializeField]
    private KeyCode launchScene1Key;
    [SerializeField]
    private KeyCode launchScene2Key;
    [SerializeField]
    private KeyCode launchScene3Key;
    [SerializeField]
    private KeyCode launchScene4Key;
    [SerializeField]
    private KeyCode launchScene5Key;

    [SerializeField]
    private KeyCode restartKey;
    [SerializeField]
    private KeyCode quitKey;

    // Use this for initialization
    void Start () {
        Debug.Log("Scene " + SceneManager.GetActiveScene().name + " launched");
    }
	
	// Update is called once per frame
	void Update () {

        //Load different scenes
        loadScene1WithKey();
        loadScene2WithKey();
        loadScene3WithKey();
        loadScene4WithKey();
        loadScene5WithKey();

        //Restart game, controller with restartKey
        restartSceneWithKey();
        //Quit game, controller with quitKey
        quitGameWithKey();

    }

    public void loadScene1()
    {
        SceneManager.LoadScene(sceneName1);
    }

    public void loadScene2()
    {
        SceneManager.LoadScene(sceneName2);
    }

    public void loadScene3()
    {
        SceneManager.LoadScene(sceneName3);
    }
    
    public void loadScene4()
    {
        SceneManager.LoadScene(sceneName4);
    }

    public void loadScene5()
    {
        SceneManager.LoadScene(sceneName5);
    }

    void loadScene1WithKey()
    {
        if (Input.GetKeyDown(launchScene1Key))
        {
            loadScene1();
        }
    }

    void loadScene2WithKey()
    {
        if (Input.GetKeyDown(launchScene2Key))
        {
            loadScene2();
        }
    }

    void loadScene3WithKey()
    {
        if (Input.GetKeyDown(launchScene3Key))
        {
            loadScene3();
        }
    }

    void loadScene4WithKey()
    {
        if (Input.GetKeyDown(launchScene4Key))
        {
            loadScene4();
        }
    }

    void loadScene5WithKey()
    {
        if (Input.GetKeyDown(launchScene5Key))
        {
            loadScene5();
        }
    }

    void restartSceneWithKey()
    {
        if (Input.GetKeyDown(restartKey))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    void quitGameWithKey()
    {
        if (Input.GetKey(quitKey))
        {
            Application.Quit();
        }
    }

    public void quitGame()
    {
        Application.Quit();
    }
}
