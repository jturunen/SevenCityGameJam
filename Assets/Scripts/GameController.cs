﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameController : MonoBehaviour {

    PlayerScript player1, player2, player3, player4;

    public TextMeshPro player1CodeText, player2CodeText, player3CodeText, player4CodeText;
    public TextMeshProUGUI countdownText;

    public List<string> startTextPool = new List<string>
    {
        "Draw!",
        "Fire!",
        "Fight!",
        "Bang!",
        "Action!",
        "Shoot!",
        "Kill!",
        "Duel!",
        "Standoff!",
        "Blast!",
        "High noon!",
        "Shootin’ time!"
    };

    public bool someoneWon = false;
    public bool gameStarted = false;

    public int playerAmount;

    void Start()
    {
        Screen.SetResolution(1280, 720, true);

        player1 = GameObject.Find("p1").GetComponent<PlayerScript>();
        player2 = GameObject.Find("p2").GetComponent<PlayerScript>();
        
        player3 = GameObject.Find("p3").GetComponent<PlayerScript>();
        player4 = GameObject.Find("p4").GetComponent<PlayerScript>();
        playerAmount = PlayerPrefs.GetInt("playerAmount");

        switch (playerAmount)
        {
            case 1:
                player2.kill();
                player3.kill();
                player4.kill();
                break;
            case 2:
                player3.kill();
                player4.kill();
                break;
            case 3:
                player4.kill();
                break;
            case 4:
                break;
            default:
                break;
        }


        player1CodeText.gameObject.SetActive(false);
        player2CodeText.gameObject.SetActive(false);
        
        player3CodeText.gameObject.SetActive(false);
        player4CodeText.gameObject.SetActive(false);
        
        StartCoroutine(Countdown((int)Random.Range(2f, 6f)));
    }




    // Update is called once per frame
    void Update()
    {
        if (!someoneWon)
        {
            if (!player1.dead && player2.dead && player3.dead && player4.dead) //player3.dead && player4.dead
            {
                winGame(player1);
            }
            else if (player1.dead && !player2.dead && player3.dead && player4.dead) //player3.dead && player4.dead
            {
                winGame(player2);
            }
            else if (player1.dead && player2.dead && !player3.dead && player4.dead)
            {
                winGame(player3);
            }
            else if (player1.dead && player2.dead && player3.dead && !player4.dead)
            {
                winGame(player4);
            }
        }
    }

    private void winGame(PlayerScript player)
    {
        someoneWon = true;
        player.canType = false;
        player.playerInputText.text = "You win!";
        
        AudioManager.instance.sfxSource.Stop();
        AudioManager.instance.musicSource.Stop();

        StartCoroutine(player.transcendWaitForAudio());
    }

    IEnumerator Countdown(int seconds)
    {
        float count = seconds;

        while (count > 0)
        {
            countdownText.text = "Ready";
            // display something...
            yield return new WaitForSeconds(1);
            count--;
        }

        // count down is finished...
        StartCoroutine(draw());
    }

    IEnumerator draw()
    {
        AudioManager.instance.PlaySingle(AudioManager.instance.startSFX);

        countdownText.text = startTextPool[Random.Range(0, startTextPool.Count)];

        yield return new WaitForSeconds(1);

        StartGame();
    }

    void StartGame()
    {
        gameStarted = true;
        countdownText.gameObject.SetActive(false);

        AudioManager.instance.PlayLoopMusic(AudioManager.instance.combatMusic);

        player1CodeText.gameObject.SetActive(true);
        player2CodeText.gameObject.SetActive(true);
        
        player3CodeText.gameObject.SetActive(true);
        player4CodeText.gameObject.SetActive(true);

        player1.indicatorHolder.SetActive(true);
        player2.indicatorHolder.SetActive(true);

        player3.indicatorHolder.SetActive(true);
        player4.indicatorHolder.SetActive(true);

        player1.canType = true;
        player2.canType = true;
        
        player3.canType = true;
        player4.canType = true;
        
    }
}
