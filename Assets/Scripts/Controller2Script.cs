﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller2Script : MonoBehaviour {
    public CodeGenerator codeGenerator;
    public SceneController sceneController;

    public ControllerScript p1;

    public List<string> code;

    [SerializeField]
    List<string> p1Code;

    [SerializeField]
    List<string> playerInput;

    [SerializeField]
    int inputCounter = 0;
    // Use this for initialization
    void Start()
    {
        playerInput = new List<string>();
        code = codeGenerator.p2Code;

        p1Code = codeGenerator.p1Code;
    }
    private void addToInput(string input)
    {
        playerInput.Add(input);
        inputCounter++;
    }
    // Update is called once per frame
    void Update()
    {

        if (Input.GetButtonDown("Con2B"))
        {
            Debug.Log("2B Rooting");
            addToInput("b");
        }

        if (Input.GetButtonDown("Con2X"))
        {
            Debug.Log("2X Tooting");
            addToInput("x");

        }

        if (Input.GetButtonDown("Con2A"))
        {
            Debug.Log("2A Shooting");
            addToInput("a");

        }

        if (Input.GetButtonDown("Con2Y"))
        {
            Debug.Log("2Y Booting");
            addToInput("y");
        }
        if (inputCounter == 6)
        {
            if (codeGenerator.listToString(p1Code) == codeGenerator.listToString(playerInput))
            {
                sceneController.loadScene4();
                Debug.Log(" 2 WinGame");
            }
        }
    }
}
