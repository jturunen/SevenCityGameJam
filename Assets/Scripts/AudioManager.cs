﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//AudioManager.instance.PlaySingle(AudioManager.instance.wolfHowl);

public class AudioManager : MonoBehaviour {
    public static AudioManager instance = null;     //Allows other scripts to call functions from SoundManager.             

    public AudioSource sfxSource, musicSource;

    public AudioClip menuMusic, combatMusic, winMusic, windBg, transcend;
    public AudioClip missTypeSFX, startSFX, menuchoiseSFX;
    public AudioClip[] inputSFXPool;

    public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
    public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.

    public AudioClip[] player1WinPool, player2WinPool, player3WinPool, player4WinPool;
    public AudioClip[] shoot1SFXPool, shoot2SFXPool, shoot3SFXPool, shoot4SFXPool;

    public bool playAmbients = false;
    public float soundRate = 5;
    public float nextSound;

    public float stepSoundRate = 0.1f;
    private float nextStepSound;
    private float audio2Volume = 0.0f;

    // Use this for initialization
    void Awake()
    {
        //Check if there is already an instance of SoundManager
        if (instance == null)
        {
            //if not, set it to this.
            instance = this;
        }
        //If instance already exists:
        else if (instance != this)
        {
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);
        }
        player1WinPool = Resources.LoadAll<AudioClip>("Audio/Player1/Win/");
        player2WinPool = Resources.LoadAll<AudioClip>("Audio/Player2/Win/");
        player3WinPool = Resources.LoadAll<AudioClip>("Audio/Player3/Win/");
        player4WinPool = Resources.LoadAll<AudioClip>("Audio/Player4/Win/");

        shoot1SFXPool = Resources.LoadAll<AudioClip>("Audio/Player1/Shoot/");
        shoot2SFXPool = Resources.LoadAll<AudioClip>("Audio/Player2/Shoot/");
        shoot3SFXPool = Resources.LoadAll<AudioClip>("Audio/Player3/Shoot/");
        shoot4SFXPool = Resources.LoadAll<AudioClip>("Audio/Player4/Shoot/");

        menuchoiseSFX = Resources.Load<AudioClip>("Audio/SFX/click_cock");
        missTypeSFX = Resources.Load<AudioClip>("Audio/SFX/Error/Arcade_Error_1");
        startSFX = Resources.Load<AudioClip>("Audio/SFX/start");
        inputSFXPool = Resources.LoadAll<AudioClip>("Audio/SFX/Handling/");

        menuMusic = Resources.Load<AudioClip>("Audio/Music/menu");
        combatMusic = Resources.Load<AudioClip>("Audio/Music/combat");
        winMusic = Resources.Load<AudioClip>("Audio/Music/win");
        windBg = Resources.Load<AudioClip>("Audio/SFX/wind");
        transcend = Resources.Load<AudioClip>("Audio/SFX/transcend");

        nextSound = Time.time + soundRate;
    }


    // Update is called once per frame
    void Update () {
        //fadeIn(musicSource);
    }

    public AudioClip getRandomSFX(AudioClip[] pool)
    {
        AudioClip audio = pool[Random.Range(0, pool.Length)];
        return audio;
    }


    //Used to play single sound clips from other sources than player.
    public void PlaySingle(AudioClip clip)
    {
        if (clip != null)
        {
            if (sfxSource.loop == true)
            {
                sfxSource.loop = false;
            }
            //Set the clip of our efxSource audio source to the clip passed in as a parameter.
            sfxSource.clip = clip;

            //Play the clip.
            sfxSource.Play();
        }
    }

    //Used to play single sound clips originating from the player (attack, eating, hurting, dying).
    public void PlaySinglePlayer(AudioClip clip, AudioSource source)
    {
        if (source.loop == true)
        {
            source.loop = false;
        }
        //Set the clip of our efxSource audio source to the clip passed in as a parameter.
        source.clip = clip;
        source.pitch = Random.Range(lowPitchRange, highPitchRange);
        //Play the clip.
        source.Play();
    }

    public void PlayLoopMusic(AudioClip clip)
    {
        /*
        audio2Volume = 0.0f;
        musicSource.volume = 0.0f;
        */
        //Set the clip of our musicSource audio source to the clip passed in as a parameter.
        musicSource.clip = clip;

        //Play the clip.
        musicSource.loop = true;
        musicSource.Play();
    }

    //RandomizeSfx chooses randomly between various audio clips and slightly changes their pitch.
    public void RandomizeSfx(params AudioClip[] clips)
    {
        //Generate a random number between 0 and the length of our array of clips passed in.
        int randomIndex = Random.Range(0, clips.Length);

        //Choose a random pitch to play back our clip at between our high and low pitch ranges.
        float randomPitch = Random.Range(lowPitchRange, highPitchRange);

        //Set the pitch of the audio source to the randomly chosen pitch.
        sfxSource.pitch = randomPitch;

        //Set the clip to the clip at our randomly chosen index.
        sfxSource.clip = clips[randomIndex];

        //Play the clip.
        sfxSource.Play();
    }

    public void fadeIn(AudioSource audioSource)
    {
        if (audioSource.volume < 1)
        {
            audio2Volume += 0.3f * Time.deltaTime;
            audioSource.volume = audio2Volume;
        }
    }
}
