﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour {

    public float speed;

    private Rigidbody rb;

    public float jumpSpeed = 8.0f;
    public float gravity = 20.0f;

    private Vector3 moveDirection = Vector3.zero;
    private bool isGrounded = true;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        //basicMoveBall();
        //basic2DMove();
        //basicMoveWithJump();
        basic2dMoveWithJump();
    }

    private void basicMoveBall()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    private void basic2DMove()
    {
        Vector3 move = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
        transform.position += move * speed * Time.deltaTime;
    }

    private void basicMoveWithJump()
    {

        if (isGrounded)
        {
            // We are grounded, so recalculate
            // move direction directly from axes

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;

            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        //moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        transform.position += moveDirection * Time.deltaTime;
    }


    private void basic2dMoveWithJump()
    {
        if(transform.position.y < 0)
        {
            isGrounded = true; 
        } else
        {
            isGrounded = false;
        }

        // We are grounded, so recalculate
        // move direction directly from axes
        if (isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f);
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection = moveDirection * speed;
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        } 

        //moveDirection.y = moveDirection.y - (gravity * Time.deltaTime);

        transform.position += moveDirection * Time.deltaTime;
    }
}
