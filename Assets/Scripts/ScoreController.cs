﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour {

    public static ScoreController instance;

    private float score;

	// Use this for initialization
	void Start () {
        instance = this;

        //Add new round to indicate the position to the scoreBoard
        //Happens everytime this is launched
        int currentRounds = PlayerPrefs.GetInt("Rounds");
        currentRounds++;
        PlayerPrefs.SetInt("Rounds", currentRounds);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void increaseScore()
    {
        score++;
    }

    public void addScoresToPrefs()
    {
        //Add current score to list to put it into the scoreboard
        int currentRounds = PlayerPrefs.GetInt("Rounds");
        PlayerPrefs.SetFloat("Score" + currentRounds, score);

        //Add current score to YourScore to be easier find
        PlayerPrefs.SetFloat("YourScore", score);
    }

}
